# Dissenter Extension

The internet's comment section. Comment on any web page on the internet.

Gab removed the download link for the Dissenter extension from their website, so here it is. It still works! :)

Comment on any web page. Bypass censorship.

Compatible with 
* Firefox
* Chrome
* Edge
* Opera

![dissenter extension screenshot](https://desbest.com/content/projects/dissenter-extension/gab%20extension%20screenshot.png)